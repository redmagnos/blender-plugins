import bpy
import bmesh

bl_info = {
    "name": "Shape Key Copy",
    "author": "Red",
    "version": (1, 0),
    "blender": (4, 0, 2),
    "location": "View3D > Toolbar > Copy Shape Keys",
    "description": "Uses surface deform to follow and copy another mesh's shape keys.",
    "warning": "",
    "wiki_url": "",
    "category": "",
}


class CopyShapeKeys(bpy.types.Operator):
    """Copy shape keys"""

    bl_label = ""
    bl_idname = "copier.shape_key_copy"
    use_filter_folder = True

    def execute(self, context):
        origin_object = bpy.context.scene.origin_object
        target_object = bpy.context.scene.target_object
        include_existing = bpy.context.scene.include_existing

        if origin_object is None or target_object is None:
            raise ValueError("You must select an origin object and a target object.")

        for obj in bpy.context.scene.objects:
            obj.select_set(False)
        origin_object.hide_set(False)
        target_object.hide_set(False)

        # ---
        # Check whether the origin mesh has shape keys
        bpy.context.view_layer.objects.active = origin_object
        origin_object.select_set(True)
        origin_shape_keys = origin_object.data.shape_keys
        if origin_shape_keys is None or len(origin_shape_keys.key_blocks) <= 1:
            raise ValueError("Origin mesh has no shape keys.")
        origin_object.select_set(False)

        # ---
        # Check whether the target mesh has concave polys that need to be fixed
        bpy.context.view_layer.objects.active = origin_object
        origin_object.select_set(True)
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type="FACE")

        bm = bmesh.from_edit_mesh(origin_object.data)
        bm.faces.active = None
        all_faces_are_convex = True
        for face in bm.faces:
            face.select_set(False)
            for loop in face.loops:
                if not loop.is_convex:
                    face.select_set(True)
                    all_faces_are_convex = False
                    break
        bmesh.update_edit_mesh(origin_object.data)

        if not all_faces_are_convex:
            raise ValueError(
                "Origin mesh has concave faces. They have been selected; please fix them before trying again. Triangulating the mesh will often fix issues like this."
            )
        bpy.ops.object.mode_set(mode="OBJECT")
        origin_object.select_set(False)

        # ---
        # Create a copy of the origin mesh and ensure none of its edges have more than two polys attached
        origin_copy = origin_object.copy()
        origin_object.select_set(False)
        origin_copy.name = "Origin Copy"
        bpy.context.collection.objects.link(origin_copy)
        origin_copy.data = origin_copy.data.copy()
        if origin_copy.animation_data:
            origin_copy.animation_data.action = origin_copy.animation_data.action.copy()

        bpy.context.view_layer.objects.active = origin_copy
        origin_copy.select_set(True)
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type="FACE")
        bpy.ops.mesh.select_all(action="DESELECT")

        faces_to_split = [1]
        while len(faces_to_split) > 0:
            faces_to_split = []
            bm = bmesh.from_edit_mesh(origin_copy.data)
            for e in bm.edges:
                faces = e.link_faces
                if len(faces) > 2:
                    faces_to_split = faces_to_split + [f.index for f in faces]

            faces_to_split = list(set(faces_to_split))
            for idx in faces_to_split:
                bm = bmesh.from_edit_mesh(origin_copy.data)
                bm.faces.ensure_lookup_table()
                bm.faces[idx].select = True
                bmesh.update_edit_mesh(origin_copy.data)
                bpy.ops.mesh.split()
                bm = bmesh.from_edit_mesh(origin_copy.data)
                bm.faces.ensure_lookup_table()
                bm.faces[idx].select = False
                bmesh.update_edit_mesh(origin_copy.data)

            bpy.ops.mesh.select_all(action="DESELECT")

        bpy.ops.object.mode_set(mode="OBJECT")
        origin_copy.select_set(False)

        # ---
        # Create a copy of the target mesh
        target_copy = target_object.copy()
        target_copy.name = "Target Copy"
        bpy.context.collection.objects.link(target_copy)
        target_copy.data = target_copy.data.copy()
        if target_copy.animation_data:
            target_copy.animation_data.action = target_copy.animation_data.action.copy()
        bpy.context.view_layer.objects.active = target_copy

        target_copy.select_set(True)
        if (
            target_copy.data.shape_keys is not None
            and len(target_copy.data.shape_keys.key_blocks) > 0
        ):
            bpy.ops.object.shape_key_remove(all=True, apply_mix=False)
        target_object.select_set(True)

        # ---
        # Add the surface deform modifier
        bpy.ops.object.modifier_add(type="SURFACE_DEFORM")
        bpy.ops.object.modifier_move_to_index(modifier="SurfaceDeform", index=0)
        bpy.context.object.modifiers["SurfaceDeform"].target = origin_copy
        bpy.ops.object.surfacedeform_bind(modifier="SurfaceDeform")
        target_copy.select_set(False)

        # ---
        # Enable the shape keys in the origin object one by one and join the copy of the target mesh
        # into the original as a shape key for each of them
        bpy.context.view_layer.objects.active = origin_copy
        origin_copy.select_set(True)
        origin_shape_keys = origin_copy.data.shape_keys
        bpy.context.object.active_shape_key_index = 0

        bpy.ops.object.editmode_toggle()
        bpy.ops.object.editmode_toggle()
        initial_target_shapes = (
            []
            if target_object.data.shape_keys is None
            else target_object.data.shape_keys.key_blocks.keys()
        )
        for i in range(1, len(origin_shape_keys.key_blocks)):
            shape_key = origin_shape_keys.key_blocks[i]
            initial_value = shape_key.value
            if (
                include_existing or shape_key.name not in initial_target_shapes
            ) and initial_value < 1:
                shape_key.value = 1

                bpy.context.view_layer.objects.active = target_object
                target_object.select_set(True)
                target_copy.select_set(True)
                bpy.ops.object.join_shapes()
                target_object.select_set(False)
                target_copy.select_set(False)
                target_object.data.shape_keys.key_blocks["Target Copy"].name = (
                    shape_key.name
                )

                bpy.context.view_layer.objects.active = origin_copy
                origin_copy.select_set(True)
                shape_key.value = initial_value

        target_copy.select_set(False)
        bpy.context.view_layer.objects.active = origin_copy
        origin_copy.select_set(True)
        bpy.ops.object.delete(use_global=False)

        bpy.context.view_layer.objects.active = target_copy
        target_copy.select_set(True)
        bpy.ops.object.delete(use_global=False)

        bpy.context.view_layer.objects.active = target_object
        target_object.select_set(True)

        bpy.ops.copier.ok_dialog("INVOKE_DEFAULT")

        return {"FINISHED"}


class OK_DIALOG(bpy.types.Operator):
    """OK"""

    bl_label = "Successfully copied shape keys."
    bl_idname = "copier.ok_dialog"

    def execute(self, context):
        return {"FINISHED"}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


bpy.types.Scene.origin_object = bpy.props.PointerProperty(
    type=bpy.types.Object,
    name="Origin",
    description="The object shape keys will be copied from",
)
bpy.types.Scene.target_object = bpy.props.PointerProperty(
    type=bpy.types.Object,
    name="Target",
    description="The object shape keys will be copied to",
)
bpy.types.Scene.include_existing = bpy.props.BoolProperty(
    name="   Include existing keys",
    description="Copies keys that already exist in the target object (this will rename the existing ones)",
    default=False,
)


class MainPanel(bpy.types.Panel):
    """
    The main panel with the buttons and inputs.
    """

    bl_label = "Copy Shape Keys"
    bl_idname = "copier.main"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Shape Key Copier"

    def draw(self, context):
        layout = self.layout
        layout.scale_y = 1.2
        layout.scale_x = 1.2

        layout.prop(context.scene, "origin_object")
        layout.prop(context.scene, "target_object")
        layout.prop(context.scene, "include_existing")

        layout.operator("copier.shape_key_copy", text="Copy", icon="FILE")


def register():
    bpy.utils.register_class(MainPanel)
    bpy.utils.register_class(CopyShapeKeys)
    bpy.utils.register_class(OK_DIALOG)


def unregister():
    bpy.utils.unregister_class(MainPanel)
    bpy.utils.unregister_class(CopyShapeKeys)
    bpy.utils.unregister_class(OK_DIALOG)


#
# This is required in order for the script to run in the text editor
if __name__ == "__main__":
    register()
