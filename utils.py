import bpy
import bmesh

bl_info = {
    "name": "Utils",
    "author": "Red",
    "version": (1, 0),
    "blender": (4, 0, 2),
    "location": "View3D > Toolbar > Utils",
    "description": "Various utilities.",
    "warning": "",
    "wiki_url": "",
    "category": "",
}


class SUBOBJECT_SUBDIVIDE(bpy.types.Operator):
    """Apply the Subdivision Surface procedure to the selected faces of the selected object, keeping its shape keys untouched"""

    """
    This module applies the Subdivision Surface modifier only to the faces currently selected in Edit Mode
    while making sure the edges of the subdivided faces still match their original positions (if they were originally
    merged) without breaking its shape keys.

    If no faces or all faces are selected, subdivides the entire object without breaking its shape keys.

    It does this by:
     - recording the original boundary vertices of the selection
     - splitting the selection into its own mesh
     - making a copy of this split mesh for each shape key
     - applying those shape keys to each respective copy
     - applying a Subdivision Surface modifier to each of them
     - joining the extra shape-key-specific copies as new shape keys of the main mesh
     - restoring the boundary vertices' positions
     - and deleting any extra vertices created that do not match the original mesh.
    """

    bl_label = ""
    bl_idname = "utils.subobject_subsurf"
    use_filter_folder = True

    def execute(self, context):
        selected_object = bpy.context.selected_objects
        if len(selected_object) != 1:
            raise ValueError("Please select exactly one object.")
        selected_object = selected_object[0]

        # Make sure the selected object is also the active object and set up Edit Mode
        bpy.context.view_layer.objects.active = selected_object
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type="FACE")

        # I will be repeating this command a lot because bmesh is a clunky library
        bm = bmesh.from_edit_mesh(selected_object.data)

        # Check whether I'm subsurfing the whole mesh or just a subset of it
        # and store data about the boundary vertices
        mesh_faces_selection = [f.select for f in bm.faces]
        any_faces_selected = any(x for x in mesh_faces_selection)
        all_faces_selected = all(x for x in mesh_faces_selection)
        if not any_faces_selected or all_faces_selected:
            # Make a list of all vertices that were already boundaries (i.e. not merged to unselected parts of the mesh)
            original_boundary = [
                v.index
                for v in bm.verts
                if v.is_boundary and v.select and all(f.select for f in v.link_faces)
            ]

            # and of all vertices that are corners (i.e. connected to only two selected edges)
            bm.verts.ensure_lookup_table()
            original_corner = [
                idx for idx in original_boundary if len(bm.verts[idx].link_edges) == 2
            ]

            # Create vertex groups to help find these vertices after application of the Subdivision Surface modifier
            bpy.ops.object.editmode_toggle()
            boundary_verts_helper_group = selected_object.vertex_groups.new(
                name="boundary_verts_helper_group"
            )
            boundary_verts_helper_group.add(original_boundary, 1, "REPLACE")
            corner_verts_helper_group = selected_object.vertex_groups.new(
                name="corner_verts_helper_group"
            )
            corner_verts_helper_group.add(original_corner, 1, "REPLACE")
            bpy.ops.object.editmode_toggle()

            # Split the selection into its own mesh
            bpy.ops.mesh.separate(type="SELECTED")
            bpy.ops.object.editmode_toggle()
            bpy.context.selected_objects[0].select_set(False)
            new_mesh = bpy.context.selected_objects[0]
            bpy.context.view_layer.objects.active = new_mesh
            selected_object.vertex_groups.remove(boundary_verts_helper_group)
            selected_object.vertex_groups.remove(corner_verts_helper_group)

            # Get the indices and positions of every new boundary vertex and every old corner vertex
            bpy.ops.object.editmode_toggle()
            bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type="VERT")
            bpy.ops.mesh.select_all(action="SELECT")
            bpy.ops.mesh.region_to_loop()
            bm = bmesh.from_edit_mesh(new_mesh.data)
            boundary_vertices = {}
            boundary_to_exclude = []
            corner_vertices = {}
            boundary_verts_helper_group = new_mesh.vertex_groups[
                "boundary_verts_helper_group"
            ]
            corner_verts_helper_group = new_mesh.vertex_groups[
                "corner_verts_helper_group"
            ]

            for v in bm.verts:
                vertex_groups = [
                    g.group for g in new_mesh.data.vertices[v.index].groups
                ]
                if v.select and v.is_boundary:
                    if (
                        boundary_verts_helper_group.index in vertex_groups
                        and boundary_verts_helper_group.weight(v.index) > 0
                    ):
                        # Vertices that have remained boundaries since before the split need to be special-case not edited
                        boundary_to_exclude.append(v.index)
                    else:
                        # Vertices that are now boundaries but did not use to be will need to be modified afterwards
                        boundary_vertices[v.index] = new_mesh.matrix_world @ v.co

                if (
                    corner_verts_helper_group.index in vertex_groups
                    and corner_verts_helper_group.weight(v.index) > 0.9
                ):
                    corner_vertices[v.index] = new_mesh.matrix_world @ v.co

            if len(boundary_to_exclude) > 0:
                # Make sure boundary_verts_helper_group is only applied to old boundary vertices
                all_verts = [v.index for v in bm.verts]
                bpy.ops.object.editmode_toggle()
                boundary_verts_helper_group.add(all_verts, 0, "REPLACE")
                boundary_verts_helper_group.add(boundary_to_exclude, 1, "REPLACE")
                bpy.ops.object.editmode_toggle()
        else:
            corner_vertices = {}
            for v in bm.verts:
                if v.is_boundary and len(v.link_edges) == 2:
                    corner_vertices[v.index] = selected_object.matrix_world @ v.co
            bpy.ops.object.editmode_toggle()
            corner_verts_helper_group = selected_object.vertex_groups.new(
                name="corner_verts_helper_group"
            )
            corner_verts_helper_group.add(
                [k for k in corner_vertices.keys()], 1, "REPLACE"
            )
            bpy.ops.object.editmode_toggle()

            new_mesh = selected_object

        # Create Subdivision Surface modifier
        bpy.ops.object.editmode_toggle()
        bpy.ops.object.modifier_add(type="SUBSURF")
        bpy.ops.object.modifier_move_to_index(modifier="Subdivision", index=0)

        # Create copies for each shape key and apply the modifier to them
        shape_keys = new_mesh.data.shape_keys
        shape_key_copies = []
        if shape_keys is not None and len(shape_keys.key_blocks) > 1:
            bpy.context.object.active_shape_key_index = 0
            new_mesh.select_set(False)

            for i in range(1, len(shape_keys.key_blocks)):
                key_name = shape_keys.key_blocks[i].name

                mesh_copy = new_mesh.copy()
                mesh_copy.name = key_name
                bpy.context.collection.objects.link(mesh_copy)
                mesh_copy.data = mesh_copy.data.copy()
                if mesh_copy.animation_data:
                    mesh_copy.animation_data.action = (
                        mesh_copy.animation_data.action.copy()
                    )
                shape_key_copies.append(mesh_copy)

                mesh_copy.select_set(True)
                bpy.context.view_layer.objects.active = mesh_copy
                bpy.ops.object.editmode_toggle()
                bpy.ops.mesh.select_all(action="SELECT")
                bpy.ops.mesh.blend_from_shape(shape=key_name)
                bpy.ops.object.editmode_toggle()
                bpy.ops.object.shape_key_remove(all=True, apply_mix=False)
                bpy.ops.object.modifier_apply(modifier="Subdivision")
                mesh_copy.select_set(False)

            new_mesh.select_set(True)
            bpy.context.view_layer.objects.active = new_mesh
            bpy.ops.object.shape_key_remove(all=True, apply_mix=False)

        # Appli the modifier to the main object
        bpy.ops.object.modifier_apply(modifier="Subdivision")

        # If there are shape key copies, join them back into the main object as shape keys
        if len(shape_key_copies) > 0:
            for shape_key_mesh in shape_key_copies:
                shape_key_mesh.select_set(True)

            bpy.ops.object.join_shapes()  # Main mesh is already selected and focused
            new_mesh.select_set(False)
            bpy.context.view_layer.objects.active = shape_key_copies[0]
            bpy.ops.object.delete(use_global=False)

            new_mesh.select_set(True)
            bpy.context.view_layer.objects.active = new_mesh

        # Now move the boundary vertices to their right places if necessary
        bpy.ops.object.editmode_toggle()
        if any_faces_selected and not all_faces_selected:
            # Select only boundary vertices
            bm = bmesh.from_edit_mesh(new_mesh.data)
            for v in bm.verts:
                if v.select and not v.is_boundary:
                    v.select = False
            bmesh.update_edit_mesh(new_mesh.data)

            bm = bmesh.from_edit_mesh(new_mesh.data)
            vertices_to_connect = []
            boundary_verts_helper_group = new_mesh.vertex_groups[
                "boundary_verts_helper_group"
            ]
            for v in bm.verts:
                if v.select:
                    if (
                        boundary_verts_helper_group.index
                        in [g.group for g in new_mesh.data.vertices[v.index].groups]
                        and boundary_verts_helper_group.weight(v.index) > 0
                    ):
                        # Unselect vertices that have been marked as boundaries not to modify
                        v.select = False
                    elif v.index in boundary_vertices:
                        # And any vertices in this situation are vertices at the boundary that existed in
                        # the original mesh and were not boundaries before, so they get restored to their
                        # old positions and connected to an inner vertex in a zigzagging pattern
                        v.co = boundary_vertices[v.index]
                        for f in v.link_faces:
                            for v2 in f.verts:
                                if not v2.select and v2.index not in boundary_vertices:
                                    vertices_to_connect.append([v, v2])
                        v.select = False

            for vertex_pair in vertices_to_connect:
                bmesh.ops.connect_verts(bm, verts=vertex_pair)

            # Then update the mesh with the modifications
            bmesh.update_edit_mesh(new_mesh.data)

            # And dissolve all selected vertices (i.e. vertices created by the subsurf modifier that are now
            # at the border and don't match the original object's vertices)
            bpy.ops.mesh.dissolve_verts()

        # Toggle edit mode off and on to apply certain updates to the state of the script
        bpy.ops.object.editmode_toggle()
        bpy.ops.object.editmode_toggle()

        # Move the corner vertices to their right places
        bm = bmesh.from_edit_mesh(new_mesh.data)
        corner_verts_helper_group = new_mesh.vertex_groups["corner_verts_helper_group"]
        for v in bm.verts:
            if v.index in corner_vertices:
                v.co = corner_vertices[v.index]
            elif (
                corner_verts_helper_group.index
                in [g.group for g in new_mesh.data.vertices[v.index].groups]
                and corner_verts_helper_group.weight(v.index) > 0
            ):
                corner1 = None
                corner2_candidates = []
                for linked_edge in v.link_edges:
                    for link_vert in linked_edge.verts:
                        if link_vert.index in corner_vertices:
                            if corner1 is None:
                                corner1 = corner_vertices[link_vert.index]
                            else:
                                corner2_candidates.append(
                                    corner_vertices[link_vert.index]
                                )
                        elif link_vert.index != v.index:
                            corner2_candidates.append(
                                new_mesh.matrix_world @ link_vert.co
                            )

                if corner1 is not None and len(corner2_candidates) > 0:
                    current_pos = new_mesh.matrix_world @ v.co
                    new_pos = corner1
                    for corner2 in corner2_candidates:
                        candidate_new_pos = (corner1 + corner2) / 2
                        if (current_pos - new_pos).length_squared > (
                            current_pos - candidate_new_pos
                        ).length_squared:
                            new_pos = candidate_new_pos
                    v.co = new_pos

        # Then update the mesh with the modifications
        bmesh.update_edit_mesh(new_mesh.data)

        # Remove the created vertex groups
        if "boundary_verts_helper_group" in new_mesh.vertex_groups:
            boundary_verts_helper_group = new_mesh.vertex_groups[
                "boundary_verts_helper_group"
            ]
            new_mesh.vertex_groups.remove(boundary_verts_helper_group)
        new_mesh.vertex_groups.remove(corner_verts_helper_group)

        # The end
        bpy.ops.utils.subobject_subsurf_ok_dialog("INVOKE_DEFAULT")

        return {"FINISHED"}


class SUBSURF_OK_DIALOG(bpy.types.Operator):
    """OK"""

    bl_label = "Successfully applied subdivision surface."
    bl_idname = "utils.subobject_subsurf_ok_dialog"

    def execute(self, context):
        return {"FINISHED"}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


bpy.types.Scene.bone_separator = bpy.props.StringProperty(
    name="",
    description="",
    default="_",
    maxlen=1,
)


class MIRROR_MESH(bpy.types.Operator):
    """Mirror the selected object along the X axis, keeping its shape keys and adjusting its bones to ensure they're mirrored too"""

    bl_label = ""
    bl_idname = "utils.mirror_mesh"

    def execute(self, context):
        bone_separator = bpy.context.scene.bone_separator
        if bone_separator == "":
            raise ValueError("Must set a bone separator.")

        # ---
        bpy.ops.object.mode_set(mode="OBJECT")
        selected_objects = bpy.context.selected_objects
        if len(selected_objects) < 1:
            raise ValueError("You must select at least one object.")

        for selected_object in selected_objects:
            selected_object.select_set(False)

        for selected_object in selected_objects:
            # First, select all faces
            bpy.context.view_layer.objects.active = selected_object
            selected_object.select_set(True)
            bpy.ops.object.editmode_toggle()
            bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type="FACE")
            bpy.ops.mesh.select_all(action="SELECT")

            # Then get the number of vertices to know what to split
            bm = bmesh.from_edit_mesh(selected_object.data)
            num_faces = len(bm.faces)
            bpy.ops.object.editmode_toggle()

            # Add the modifier
            bpy.ops.object.modifier_add(type="MIRROR")
            bpy.ops.object.modifier_move_to_index(modifier="Mirror", index=0)
            bpy.context.object.modifiers["Mirror"].use_mirror_merge = False

            # Deal with the possible existence of shape keys before applying the modifier
            shape_keys = selected_object.data.shape_keys
            shape_key_copies = []
            if shape_keys is not None and len(shape_keys.key_blocks) > 1:
                bpy.context.object.active_shape_key_index = 0
                selected_object.select_set(False)

                for i in range(1, len(shape_keys.key_blocks)):
                    key_name = shape_keys.key_blocks[i].name

                    mesh_copy = selected_object.copy()
                    mesh_copy.name = key_name
                    bpy.context.collection.objects.link(mesh_copy)
                    mesh_copy.data = mesh_copy.data.copy()
                    if mesh_copy.animation_data:
                        mesh_copy.animation_data.action = (
                            mesh_copy.animation_data.action.copy()
                        )
                    shape_key_copies.append(mesh_copy)

                    mesh_copy.select_set(True)
                    bpy.context.view_layer.objects.active = mesh_copy
                    bpy.ops.object.editmode_toggle()
                    bpy.ops.mesh.select_all(action="SELECT")
                    bpy.ops.mesh.blend_from_shape(shape=key_name)
                    bpy.ops.object.editmode_toggle()
                    bpy.ops.object.shape_key_remove(all=True, apply_mix=False)
                    bpy.ops.object.modifier_apply(modifier="Mirror")
                    mesh_copy.select_set(False)

                selected_object.select_set(True)
                bpy.context.view_layer.objects.active = selected_object
                bpy.ops.object.shape_key_remove(all=True, apply_mix=False)

            # Apply the modifier and rejoin shape keys if needed
            bpy.ops.object.modifier_apply(modifier="Mirror")
            if len(shape_key_copies) > 0:
                for shape_key_mesh in shape_key_copies:
                    shape_key_mesh.select_set(True)

                bpy.ops.object.join_shapes()
                selected_object.select_set(False)
                bpy.context.view_layer.objects.active = shape_key_copies[0]
                bpy.ops.object.delete(use_global=False)

                selected_object.select_set(True)
                bpy.context.view_layer.objects.active = selected_object

            # Now we select all of the new vertices and split them into a new mesh
            bpy.ops.object.mode_set(mode="EDIT")
            bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type="FACE")
            bm = bmesh.from_edit_mesh(selected_object.data)
            for f in bm.faces:
                f.select = f.index >= num_faces
                print(f.select)
            bmesh.update_edit_mesh(selected_object.data)
            bpy.ops.mesh.separate(type="SELECTED")
            bpy.ops.object.editmode_toggle()
            bpy.context.selected_objects[0].select_set(False)
            new_mesh = bpy.context.selected_objects[0]
            bpy.context.view_layer.objects.active = new_mesh

            # Rename the bones in the new mesh
            for g in new_mesh.vertex_groups:
                original_name = g.name
                split_name = original_name.split(bone_separator)
                direction = split_name[-1]

                directions_dict = {"l": "r", "L": "R", "r": "l", "R": "L"}
                if (
                    direction in directions_dict
                    and bone_separator.join(split_name[0:-1])
                    + bone_separator
                    + directions_dict[direction]
                    not in new_mesh.vertex_groups
                ):
                    g.name = (
                        bone_separator.join(split_name[0:-1])
                        + bone_separator
                        + directions_dict[direction]
                    )

        return {"FINISHED"}


class EXPLODE_SHAPE_KEYS(bpy.types.Operator):
    """Create one copy of the selected object for each of its shape keys"""

    bl_label = ""
    bl_idname = "utils.explode_shape_keys"

    def execute(self, context):
        selected_object = bpy.context.selected_objects
        if len(selected_object) != 1:
            raise ValueError("You must select exactly one object.")

        bpy.ops.object.mode_set(mode="OBJECT")
        selected_object = selected_object[0]
        selected_object.select_set(True)
        bpy.context.view_layer.objects.active = selected_object

        # ---
        shape_keys = selected_object.data.shape_keys
        if shape_keys is None or len(shape_keys.key_blocks) <= 1:
            raise ValueError("Selected object must have at least one shape key.")

        selected_object.select_set(False)
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type="VERT")
        bpy.ops.object.editmode_toggle()
        for i in range(1, len(shape_keys.key_blocks)):
            key_name = shape_keys.key_blocks[i].name

            mesh_copy = selected_object.copy()
            mesh_copy.name = key_name
            bpy.context.collection.objects.link(mesh_copy)
            mesh_copy.data = mesh_copy.data.copy()
            if mesh_copy.animation_data:
                mesh_copy.animation_data.action = mesh_copy.animation_data.action.copy()

            mesh_copy.select_set(True)
            bpy.context.view_layer.objects.active = mesh_copy
            bpy.ops.object.editmode_toggle()
            bpy.ops.mesh.select_all(action="SELECT")
            bpy.ops.mesh.blend_from_shape(shape=key_name)
            bpy.ops.object.editmode_toggle()
            bpy.ops.object.shape_key_remove(all=True, apply_mix=False)
            mesh_copy.select_set(False)

        selected_object.select_set(True)
        bpy.context.view_layer.objects.active = selected_object

        bpy.ops.utils.explode_shape_keys_ok_dialog("INVOKE_DEFAULT")

        return {"FINISHED"}


class EXPLODE_SHAPE_KEYS_OK_DIALOG(bpy.types.Operator):
    """OK"""

    bl_label = "Successfully exploded shape keys"
    bl_idname = "utils.explode_shape_keys_ok_dialog"

    def execute(self, context):
        return {"FINISHED"}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


class MAIN_PANEL(bpy.types.Panel):
    """
    The main panel with the buttons and utilities.
    """

    bl_label = "Utilities"
    bl_idname = "utils.main"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Utilities"

    def draw(self, context):
        layout = self.layout
        layout.scale_y = 1.2
        layout.scale_x = 1.2

        col = layout.column()
        col.operator(
            "utils.subobject_subsurf", text="Subsurf Subobject", icon="MOD_SUBSURF"
        )

        box = layout.box()
        col = box.column()
        col.label(text="Bone Separator:")
        col.prop(context.scene, "bone_separator")
        col.operator("utils.mirror_mesh", text="Mirror Meshes", icon="MOD_MIRROR")

        col = layout.column()
        col.operator(
            "utils.explode_shape_keys", text="Explode Shape Keys", icon="SHAPEKEY_DATA"
        )


def register():
    """
    Registers the Classes with the various utilities.
    """
    bpy.utils.register_class(MAIN_PANEL)
    bpy.utils.register_class(SUBOBJECT_SUBDIVIDE)
    bpy.utils.register_class(SUBSURF_OK_DIALOG)
    bpy.utils.register_class(MIRROR_MESH)
    bpy.utils.register_class(EXPLODE_SHAPE_KEYS)
    bpy.utils.register_class(EXPLODE_SHAPE_KEYS_OK_DIALOG)


def unregister():
    """
    Registers the Classes with the various utilities.
    """
    bpy.utils.unregister_class(MAIN_PANEL)
    bpy.utils.unregister_class(SUBOBJECT_SUBDIVIDE)
    bpy.utils.unregister_class(SUBSURF_OK_DIALOG)
    bpy.utils.unregister_class(MIRROR_MESH)
    bpy.utils.unregister_class(EXPLODE_SHAPE_KEYS)
    bpy.utils.unregister_class(EXPLODE_SHAPE_KEYS_OK_DIALOG)


#
# This is required in order for the script to run in the text editor
if __name__ == "__main__":
    register()
