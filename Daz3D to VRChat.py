import bpy
import re
import copy


def replaceBones(bones, bones_to_replace, replacement_bone):
    if any(bone in bones for bone in bones_to_replace):
        if replacement_bone not in bones:
            bones.new(name=replacement_bone)

        for bone in bones_to_replace:
            if bone in bones:
                bpy.ops.object.modifier_add(type="VERTEX_WEIGHT_MIX")
                bpy.context.object.modifiers["VertexWeightMix"].vertex_group_a = (
                    replacement_bone
                )
                bpy.context.object.modifiers["VertexWeightMix"].vertex_group_b = bone
                bpy.context.object.modifiers["VertexWeightMix"].mix_set = "OR"
                bpy.context.object.modifiers["VertexWeightMix"].mix_mode = "ADD"
                bpy.ops.object.modifier_apply(modifier="VertexWeightMix")

                bones.remove(bones[bone])


if __name__ == "__main__":
    objects = bpy.context.selected_objects
    if len(objects) > 0:
        for obj in objects:
            obj.select_set(False)

        for obj in objects:
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj

            if obj.type == "ARMATURE":
                bpy.ops.object.mode_set(mode="EDIT")

                bones = obj.data.edit_bones

                # Reassign bones from alt skeletons
                bones_to_reassign = {
                    "lowerjaw": ["tongue01"],
                    "pelvis": ["scrotum"],
                }
                for parent in bones_to_reassign:
                    for child in bones_to_reassign[parent]:
                        try:
                            bones[child].parent = bones[parent]
                        except:
                            print(child + " failed to be reassigned.")
                bpy.ops.object.mode_set(mode="EDIT", toggle=True)
                bpy.ops.object.mode_set(mode="EDIT")

                # Remove unused bones from armature
                # Individual leaf bones
                bones_to_remove = [
                    "spine1",
                    "spine4",
                    "l_upperarmtwist1",
                    "l_upperarmtwist2",
                    "r_upperarmtwist1",
                    "r_upperarmtwist2",
                    "l_forearmtwist1",
                    "l_forearmtwist2",
                    "r_forearmtwist1",
                    "r_forearmtwist2",
                    "l_hand_anchor",
                    "r_hand_anchor",
                    "l_indexmetacarpal",
                    "l_midmetacarpal",
                    "l_ringmetacarpal",
                    "l_pinkymetacarpal",
                    "r_indexmetacarpal",
                    "r_midmetacarpal",
                    "r_ringmetacarpal",
                    "r_pinkymetacarpal",
                    "neck2",
                    "l_ear",
                    "r_ear",
                    "upperteeth",
                    "testicleCrease",
                    "shaftFold",
                    "l_thightwist1",
                    "l_thightwist2",
                    "l_metatarsal",
                    "r_thightwist1",
                    "r_thightwist2",
                    "r_metatarsal",
                ]

                # Bones to be removed along with their children
                bones_to_recursively_remove = [
                    "upperfacerig",
                    "lowerteeth",
                    "lowerfacerig",
                ]

                # Bones whose children to remove
                bones_to_turn_into_leaves = ["r_toes", "l_toes"]

                # First add children of leaves-to-be to bones_to_recursively_remove
                while len(bones_to_turn_into_leaves) > 0:
                    bones_to_recursively_remove += bones[
                        bones_to_turn_into_leaves.pop()
                    ].children

                # Then add all non-hip armatures to the bones to recursively remove
                for bone in bones:
                    if bone.parent is None and bone.name != "hip":
                        bones_to_recursively_remove.append(bone)

                # Next recursively go through the recursive bones to add them to bones to remove
                while len(bones_to_recursively_remove) > 0:
                    bone_or_bone_name = bones_to_recursively_remove.pop()
                    if type(bone_or_bone_name) is str:
                        try:
                            bone = bones[bone_or_bone_name]
                        except Exception:
                            continue
                    else:
                        bone = bone_or_bone_name

                    bones_to_remove += [bone]
                    bones_to_recursively_remove += bone.children

                # Finally remove all relevant bones
                for b in bones_to_remove:
                    try:
                        if type(b) is str:
                            bone_to_remove = bones[b]
                        else:
                            bone_to_remove = b
                        bones.remove(bone_to_remove)
                    except Exception:
                        print("Could not remove " + b + ".")
                bpy.ops.object.mode_set(mode="EDIT", toggle=True)
                bpy.ops.object.mode_set(mode="EDIT")

                # Move the tails of bones with individual children to those children's heads
                for b in bones:
                    if type(b) is str:
                        try:
                            bone = bones[b]
                        except Exception:
                            continue
                    else:
                        bone = b

                    if len(bone.children) == 1:
                        bone.tail = bone.children[0].head
                        bone.children[0].use_connect = True
                bpy.ops.object.mode_set(mode="EDIT", toggle=True)

            elif obj.type == "MESH":
                bones = obj.vertex_groups
                # Remove .XXXX from the end of bone names
                for bone in bones:
                    corrected_bone_name = re.sub(r"\.\d+$", "", bone.name)
                    if corrected_bone_name == bone.name:
                        # If the name needs no correction, no-op
                        continue
                    elif corrected_bone_name in bones:
                        # If the correct bone already exists, add the wrong one to the correct one and delete the wrong one
                        bpy.ops.object.modifier_add(type="VERTEX_WEIGHT_MIX")
                        bpy.context.object.modifiers[
                            "VertexWeightMix"
                        ].vertex_group_a = corrected_bone_name
                        bpy.context.object.modifiers[
                            "VertexWeightMix"
                        ].vertex_group_b = bone.name
                        bpy.context.object.modifiers["VertexWeightMix"].mix_set = "OR"
                        bpy.context.object.modifiers["VertexWeightMix"].mix_mode = "ADD"
                        bpy.ops.object.modifier_apply(modifier="VertexWeightMix")

                        bones.remove(bone)
                    else:
                        # Otherwise, just rename the wrong one
                        bone.name = corrected_bone_name

                # Remove unused bones
                bone_replacement_list = {
                    "hip": ["spine1"],
                    "pelvis": [
                        "pelvis2",
                        "legs_crease",
                        "rectum_01",
                        "rectum_02",
                        "rectum_03",
                        "rectum_04",
                        "rectum_05",
                        "rectum_06",
                        "rectum07",
                        "anus_01",
                        "anus_02",
                        "root",
                        "shaft_01",
                        "shaft_02",
                        "shaft_03",
                        "shaft_04",
                        "shaft_05",
                        "shaft_06",
                        "shaft_07",
                        "shaft_08",
                        "shaft_09",
                        "shaft_10",
                        "foreskin_01",
                        "foreskin_02",
                        "foreskin_03",
                        "foreskin_04",
                        "foreskin_05",
                        "foreskin_06",
                        "foreskin_07",
                        "foreskin_08",
                        "foreskin_09",
                        "foreskin_infold",
                        "glans",
                        "glans_tip",
                        "urethra",
                    ],
                    "scrotum": ["testicleCrease", "shaftFold"],
                    "l_thigh": ["l_thightwist1", "l_thightwist2"],
                    "l_foot": ["l_metatarsal"],
                    "l_toes": [
                        "l_bigtoe1",
                        "l_bigtoe2",
                        "l_indextoe1",
                        "l_indextoe2",
                        "l_midtoe1",
                        "l_midtoe2",
                        "l_ringtoe1",
                        "l_ringtoe2",
                        "l_pinkytoe1",
                        "l_pinkytoe2",
                    ],
                    "r_thigh": ["r_thightwist1", "r_thightwist2"],
                    "r_foot": ["r_metatarsal"],
                    "r_toes": [
                        "r_bigtoe1",
                        "r_bigtoe2",
                        "r_indextoe1",
                        "r_indextoe2",
                        "r_midtoe1",
                        "r_midtoe2",
                        "r_ringtoe1",
                        "r_ringtoe2",
                        "r_pinkytoe1",
                        "r_pinkytoe2",
                    ],
                    "spine3": ["spine4"],
                    "neck1": ["neck2"],
                    "l_hand": [
                        "l_indexmetacarpal",
                        "l_midmetacarpal",
                        "l_ringmetacarpal",
                        "l_pinkymetacarpal",
                        "l_hand_anchor",
                    ],
                    "l_forearm": ["l_forearmtwist1", "l_forearmtwist2"],
                    "l_upperarm": ["l_upperarmtwist1", "l_upperarmtwist2"],
                    "r_hand": [
                        "r_indexmetacarpal",
                        "r_midmetacarpal",
                        "r_ringmetacarpal",
                        "r_pinkymetacarpal",
                        "r_hand_anchor",
                    ],
                    "r_forearm": ["r_forearmtwist1", "r_forearmtwist2"],
                    "r_upperarm": ["r_upperarmtwist1", "r_upperarmtwist2"],
                    "head": [
                        "l_ear",
                        "r_ear",
                        "upperteeth",
                        "upperfacerig",
                        "l_browinner",
                        "l_browouter",
                        "r_browinner",
                        "r_browouter",
                        "centerbrow",
                        "l_eyelidupper",
                        "l_eyelidlower",
                        "r_eyelidupper",
                        "r_eyelidlower",
                        "l_squint",
                        "r_squint",
                        "l_cheek",
                        "r_cheek",
                        "l_nostril",
                        "r_nostril",
                        "lipuppermiddle",
                        "l_lipupper",
                        "r_lipupper",
                        "l_infraorbital",
                        "r_infraorbital",
                    ],
                    "lowerjaw": [
                        "lowerteeth",
                        "lowerfacerig",
                        "l_lipcorner",
                        "l_liplower",
                        "liplowermiddle",
                        "r_liplower",
                        "r_lipcorner",
                        "l_cheeklower",
                        "r_cheeklower",
                        "chin",
                    ],
                }
                for replacement_bone in bone_replacement_list:
                    bones_to_replace = bone_replacement_list[replacement_bone]
                    replaceBones(obj.vertex_groups, bones_to_replace, replacement_bone)

                shape_keys = obj.data.shape_keys
                if shape_keys is not None:
                    # Standardise shape key names
                    shape_key_blocks = shape_keys.key_blocks
                    for sk in shape_key_blocks:
                        if "__" in sk.name:
                            sk.name = sk.name.split("__")[1]
                        if "facs_ctrl_" in sk.name:
                            sk.name = sk.name.split("facs_ctrl_")[1]

                    # Create visemes
                    if any(
                        viseme in shape_key_blocks
                        for viseme in [
                            "vM",
                            "vTH",
                            "vT",
                            "vSH",
                            "vL",
                            "vER",
                            "vAA",
                            "vEE",
                            "vIH",
                            "vOW",
                            "vUW",
                            "vF",
                            "vK",
                            "vS",
                        ]
                    ):
                        # vSil
                        bpy.ops.object.shape_key_add(from_mix=False)
                        shape_key_blocks[-1].name = "vSil"

                        # vPP
                        shape_key_blocks["vM"].slider_max = shape_key_blocks[
                            "vM"
                        ].value = 1.5
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vPP"
                        shape_key_blocks["vM"].value = 0
                        shape_key_blocks["vM"].slider_max = 1

                        # vFF
                        shape_key_blocks["vF"].name = "vFF"

                        # vTH
                        shape_key_blocks["vTH"].slider_max = shape_key_blocks[
                            "vTH"
                        ].value = 1.6
                        bpy.ops.object.shape_key_add(from_mix=True)
                        obj.shape_key_remove(shape_key_blocks["vTH"])
                        shape_key_blocks[-1].name = "vTH"

                        # vDD
                        shape_key_blocks["vT"].slider_max = shape_key_blocks[
                            "vT"
                        ].value = 2
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vDD"
                        shape_key_blocks["vT"].value = 0
                        shape_key_blocks["vT"].slider_max = 1

                        # vKK
                        shape_key_blocks["vK"].name = "vKK"

                        # vCH
                        shape_key_blocks["vSH"].slider_max = shape_key_blocks[
                            "vSH"
                        ].value = 2
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vCH"
                        shape_key_blocks["vSH"].value = 0
                        shape_key_blocks["vSH"].slider_max = 1

                        # vSS
                        shape_key_blocks["vS"].name = "vSS"

                        # vNN
                        shape_key_blocks["vL"].slider_max = shape_key_blocks[
                            "vL"
                        ].value = 2
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vNN"
                        shape_key_blocks["vL"].value = 0
                        shape_key_blocks["vL"].slider_max = 1

                        # vRR
                        shape_key_blocks["vER"].value = 0.5
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vRR"
                        shape_key_blocks["vER"].value = 0

                        # vAA
                        shape_key_blocks["vAA"].slider_max = shape_key_blocks[
                            "vAA"
                        ].value = 2
                        bpy.ops.object.shape_key_add(from_mix=True)
                        obj.shape_key_remove(shape_key_blocks["vAA"])
                        shape_key_blocks[-1].name = "vAA"

                        # vE
                        shape_key_blocks["vEE"].slider_max = shape_key_blocks[
                            "vEE"
                        ].value = 1.5
                        bpy.ops.object.shape_key_add(from_mix=True)
                        obj.shape_key_remove(shape_key_blocks["vEE"])
                        shape_key_blocks[-1].name = "vE"

                        # vIH
                        shape_key_blocks["vIH"].slider_max = shape_key_blocks[
                            "vIH"
                        ].value = 1.5
                        bpy.ops.object.shape_key_add(from_mix=True)
                        obj.shape_key_remove(shape_key_blocks["vIH"])
                        shape_key_blocks[-1].name = "vIH"

                        # vOH
                        shape_key_blocks["vOW"].slider_max = shape_key_blocks[
                            "vOW"
                        ].value = 1.3
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vOH"
                        shape_key_blocks["vOW"].value = 0
                        shape_key_blocks["vOW"].slider_max = 1

                        # vOU
                        shape_key_blocks["vUW"].value = 0.3
                        shape_key_blocks["vW"].value = 0.8
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vOU_temp"
                        shape_key_blocks["vUW"].value = 0
                        shape_key_blocks["vW"].value = 0
                        shape_key_blocks["vOU_temp"].slider_max = shape_key_blocks[
                            "vOU_temp"
                        ].value = 1.5
                        bpy.ops.object.shape_key_add(from_mix=True)
                        shape_key_blocks[-1].name = "vOU"
                        obj.shape_key_remove(shape_key_blocks["vOU_temp"])

                    # Alphabetize shape keys
                    num_shape_keys = len(shape_key_blocks)
                    if num_shape_keys > 2:
                        shape_key_names = copy.copy(shape_key_blocks.keys())
                        shape_key_names_sorted = sorted(
                            copy.copy(shape_key_names), key=str.casefold
                        )
                        for i in range(0, num_shape_keys):
                            if shape_key_names_sorted[i] == shape_key_names[0]:
                                continue

                            idx = shape_key_names.index(shape_key_names_sorted[i])
                            bpy.context.object.active_shape_key_index = idx
                            bpy.ops.object.shape_key_move(type="BOTTOM")
                            shape_key_names.append(shape_key_names.pop(idx))

            obj.select_set(False)
